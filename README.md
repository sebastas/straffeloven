Strafeloven 2005 - 
 § 205. Krenkelse av retten til privat kommunikasjon
 Med bot eller fengsel inntil 2 år straffes den som uberettiget

 a)	og ved bruk av teknisk hjelpemiddel hemmelig avlytter eller gjør hemmelig 
 opptak av telefonsamtale eller annen kommunikasjon mellom andre, eller av 
 forhandlinger i lukket møte som han ikke selv deltar i, eller som han 
 uberettiget har skaffet seg tilgang til,
 b)	bryter en beskyttelse eller på annen uberettiget måte skaffer seg tilgang 
 til informasjon som overføres ved elektroniske eller andre tekniske 
 hjelpemidler,
 c)	åpner brev eller annen lukket skriftlig meddelelse som er adressert til en 
 annen, eller på annen måte skaffer seg uberettiget tilgang til innholdet, eller
 d)	hindrer eller forsinker adressatens mottak av en meddelelse ved å skjule, 
 endre, forvanske, ødelegge eller holde meddelelsen tilbake.
